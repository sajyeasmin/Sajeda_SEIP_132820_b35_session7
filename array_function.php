<?php
echo "<pre>";
$fname=array("Peter","Ben","Joe");
$age=array("35","37","43");

$c=array_combine($fname,$age);
print_r($c);
echo "<br>";

function test_odd($var)
{
    return($var & 1); //bit wise operator
}

$a1=array("a","b",2,3,4,5,6,7,8,9,10);
print_r(array_filter($a1,"test_odd"));
echo "<br>";

$car=array("Volvo"=>"XC90","BMW"=>"X5","Toyota"=>"Highlander");
print_r(array_keys($car));
echo "<br>";

$x=array_key_exists("Peter",$fname);
echo "Key of volvo :".$x."<br>";

$a=array("red","green");
print_r(array_pad($a,5,"blue"));
echo "<br>";

echo "Array merge"."<br>";
$a1=array("red","green");
$a2=array("blue","yellow");
$a3=array("blacl","orange");
print_r(array_merge($a1,$a2,$a3));

echo "Array pop"."<br>";
$a=array("red","green");
print_r(array_pop($a));
echo "<br>";

echo "Array random"."<br>";
$random_keys=array_rand($car,2);
echo $random_keys[0]."<br>";
echo $random_keys[1]."<br>";

echo "Array replace"."<br>";
$a1=array("red","green");
$a2=array("blue","yellow");
print_r(array_replace($a1,$a2,$a3));

echo "Array Search"."<br>";
$f=array_search("Highlander",$car);
echo $f;

echo "Array reverse"."<br>";
print_r(array_reverse($car));
echo "<br>";

echo "Array shift"."<br>";
print_r(array_shift($car));
echo "<br>";

echo "Array unshift"."<br>";
print_r(array_unshift($a1,"black","orange"));
echo "<br>";

echo "Array unique"."<br>";
$unfiltered=array(2,2,5,1, 4, 5, 4, 3, 2);
$filtered=array_unique($unfiltered);
print_r(array_values($filtered));
echo "<br>";

echo "Array compact"."<br>";
$firstname = "Peter";
$lastname = "Griffin";
$age = "41";
$result = compact("firstname","lastname","age");
print_r($result);
echo "<br>";

$people = array("Peter", "Joe", "Glenn", "Cleveland");
print_r (each($people));

echo "</pre>";